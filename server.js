/**
 * Created by instancetype on 6/7/14.
 */
var https = require('https'),
    fs = require('fs');

var options = {
    // This location is UNSAFE and for convenient demo purposes only
    key : fs.readFileSync('./key.pem'),
    cert : fs.readFileSync('./key-cert.pem')
};

https.createServer(options, function(req, res) {
    res.writeHead(200);
    res.end('hello world\n');
}).listen(3000);